const muridFsw13 = [
  {
    nama: "Gusde",
    asal: "Bali",
    umur: 23,
    status: "menikah",
  },
  {
    nama: "Chandra",
    asal: "Jawa Tengah",
    umur: 27,
    status: "duda",
  },
  {
    nama: "Laila",
    asal: "Jawa Timur",
    umur: 20,
    status: "single",
  },
  {
    nama: "Narantyo",
    asal: "Jawa Tengah",
    umur: 25,
    status: "single",
  },
  {
    nama: "Nilam",
    asal: "Jawa Barat",
    umur: 30,
    status: "Menikah",
  },
];

function callback(dataMurid, cbAsal, cbUmur, cbStatus) {
  const hasil = [];
  for (let i = 0; i < dataMurid.length; i++) {
    hasil.push(
      "Nama saya " +
        dataMurid[i].nama +
        ", " +
        cbAsal(dataMurid[i].asal) +
        ", " +
        cbUmur(dataMurid[i].umur) +
        ", " +
        cbStatus(dataMurid[i].status)
    );
  }
  return hasil;
}

const data = callback(
  muridFsw13,
  (dataAsal) => {
    if (dataAsal == "Jawa Barat") {
      return "saya tinggal di " + dataAsal;
    } else {
      return "saya bukan dari Jawa Barat tetapi dari " + dataAsal;
    }
  },
  (dataUmur) => {
    if (dataUmur > 22) {
      return "umur saya diatas 22 tahun yaitu " + dataUmur;
    } else if (dataUmur < 22) {
      return "umur saya dibawah 22 tahun yaitu " + dataUmur;
    }
  },
  (dataStatus) => {
    let hasil;
    if (dataStatus == "single") {
      return "saya masih " + dataStatus;
    } else {
      return "saya sudah " + dataStatus;
    }
  }
);

console.log(data);
